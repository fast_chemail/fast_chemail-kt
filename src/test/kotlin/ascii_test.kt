// Copyright 2016  Jonas Me
// See the 'AUTHORS' file at the top-level directory for a full list of authors.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package fast_chemail

import org.junit.Assert
import org.junit.Test

class TestIsLetter {
    @Test
    fun f() {
        Assert.assertTrue(isLetter('a'))
    }
}
