// Copyright 2016  Jonas Me
// See the 'AUTHORS' file at the top-level directory for a full list of authors.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package fast_chemail

internal fun isLetter(c: Char): Boolean {
    if (c in 'a'..'z' || c in 'A'..'Z') {
        return true
    }
    return false
}

internal fun isDigit(c: Char): Boolean {
    return c in '0'..'9'
}

// `checkAsciiPrintable` reports an error wheter the string has a non-ASCII
// character or any ASCII control character.
internal fun checkAsciiPrintable(name: String) {
    var i: Int = 1

    for (char in name) {
        when (char.toInt()) {
            in 0x20..0x7E -> Unit
            in 0x00..0x1F, 0x7F -> throw ControlCharException(i)
            else -> throw NonAsciiException(char)
        }
        i++
    }
}

// == Errors
//

class NonAsciiException(val c: Char): Exception() {
    override val message: String = "contain non US-ASCII character ($c)"
}

class ControlCharException(val position: Int): Exception() {
    override val message: String = "contain ASCII control character at position $position"
}
