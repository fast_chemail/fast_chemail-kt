// Copyright 2016  Jonas Me
// See the 'AUTHORS' file at the top-level directory for a full list of authors.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package fast_chemail

// https://tools.ietf.org/html/rfc5321#section-4.5.3.1
//
// The maximum total length of a user name or other local-part is 64 octets.
// The maximum total length of a domain name or number is 255 octets.
private const val MAX_LOCAL_PART = 64
private const val MAX_DOMAIN_PART = 255
private const val MAX_LABEL = 63

// `isValidEmail` checks wheter an email address is valid.
fun isValidEmail(address: String): Boolean {
    try {
        parseEmail(address)
    } catch (e: Throwable) {
        return false
    }
    return true
}

// `parseEmail` scans an email address to check wheter it is correct.
fun parseEmail(address: String) {
    if (address.startsWith('@')) {
        throw NoLocalPartException()
    }
    if (address.endsWith('@')) {
        throw NoDomainPartException()
    }

    // https://tools.ietf.org/html/rfc5321#section-4.1.2
    //
    // Systems MUST NOT define mailboxes in such a way as to require the use in
    // SMTP of non-ASCII characters (octets with the high order bit set to one)
    // or ASCII "control characters" (decimal value 0-31 and 127).
    try {
        checkAsciiPrintable(address)
    } catch (e: Throwable) {
        throw e
    }

    val parts = address.split('@');
    if (parts.size != 2) {
        if (parts.size > 2) {
            throw TooAtException()
        }
        throw NoSignAtException()
    }

    // == Local part

    // https://tools.ietf.org/html/rfc3696#section-3
    //
    // Period (".") may also appear, but may not be used to start or end
    // the local part, nor may two or more consecutive periods appear.

    if (parts[0].length > MAX_LOCAL_PART) {
        throw LocalTooLongException()
    }
    if (parts[0].startsWith('.')) {
        throw LocalStartPeriodException()
    }
    if (parts[0].endsWith('.')) {
        throw LocalEndPeriodException()
    }

    var lastPeriod = false
    for (char in parts[0]) {
        if (isLetter(char) || isDigit(char)) {
            if (lastPeriod) {
                lastPeriod = false
            }
            continue
        }

        when (char) {
            '.' -> {
                if (lastPeriod) {
                    throw ConsecutivePeriodException()
                }
                lastPeriod = true
            }
            '!', '#', '$', '%', '&', '\'', '*', '+', '-', '/', '=', '?', '^',
                '_', '`', '{', '|', '}', '~' -> {
                // atom :: https://tools.ietf.org/html/rfc5322#section-3.2.3
                if (lastPeriod) {
                    lastPeriod = false
                }
            }
            else -> throw WrongCharLocalException(char)
        }
    }

    // == Domain part

    // https://tools.ietf.org/html/rfc5321#section-4.1.2
    //
    // characters outside the set of alphabetic characters, digits, and hyphen
    // MUST NOT appear in domain name labels for SMTP clients or servers.  In
    // particular, the underscore character is not permitted.

    // https://tools.ietf.org/html/rfc1034#section-3.5
    //
    // The labels must follow the rules for ARPANET host names.  They must start
    // with a letter, end with a letter or digit, and have as interior
    // characters only letters, digits, and hyphen.  There are also some
    // restrictions on the length.  Labels must be 63 characters or less.

    // https://tools.ietf.org/html/rfc3696#section-2
    //
    // It is likely that the better strategy has now become to make the "at
    // least one period" test, to verify LDH conformance (including verification
    // that the apparent TLD name is not all-numeric), and then to use the DNS
    // to determine domain name validity, rather than trying to maintain a local
    // list of valid TLD names.

    if (parts[1].length > MAX_DOMAIN_PART) {
        throw DomainTooLongException()
    }
    if (parts[1].startsWith('.')) {
        throw DomainStartPeriodException()
    }
    if (parts[1].endsWith('.')) {
        throw DomainEndPeriodException()
    }

    val labels = parts[1].split('.')
    if (labels.size == 1) {
        throw NoPeriodDomainException()
    }

    // label = let-dig [ [ ldh-str ] let-dig ]
	// limited to a length of 63 characters by RFC 1034 section 3.5
	//
	// <let-dig> ::= <letter> | <digit>
	// <ldh-str> ::= <let-dig-hyp> | <let-dig-hyp> <ldh-str>
	// <let-dig-hyp> ::= <let-dig> | "-"
    for (label in labels) {
        if (label.length == 0) {
            throw ConsecutivePeriodException()
        }
        if (label.length > MAX_LABEL) {
            throw LabelTooLongException()
        }

        for (char in label) {
            if (isLetter(char) || isDigit(char) || char == '-') {
                continue
            }
            throw WrongCharDomainException(char)
        }

        val firstChar = label[0]
        if (!isLetter(firstChar) && !isDigit(firstChar)) {
            throw WrongStartLabelException(label[0])
        }
        val lastChar = label[label.length - 1]
        if (!isLetter(lastChar) && !isDigit(lastChar)) {
            throw WrongEndLabelException(lastChar)
        }
    }
}

// == Errors
//

class NoLocalPartException: Exception() {
    override val message: String = "no local part"
}

class NoDomainPartException: Exception() {
    override val message: String = "no domain part"
}

class NoSignAtException: Exception() {
    override val message: String = "no at sign (@)"
}

class TooAtException: Exception() {
    override val message: String = "wrong number of at sign (@)"
}

class LocalTooLongException: Exception() {
    override val message: String = "the local part has more than $MAX_LOCAL_PART characters"
}

class DomainTooLongException: Exception() {
    override val message: String = "the domain part has more than $MAX_DOMAIN_PART characters"
}

class LabelTooLongException: Exception() {
    override val message: String = "a domain label has more than $MAX_LABEL characters"
}

class LocalStartPeriodException: Exception() {
    override val message: String = "the local part starts with a period"
}

class LocalEndPeriodException: Exception() {
    override val message: String = "the local part ends with a period"
}

class DomainStartPeriodException: Exception() {
    override val message: String = "the domain part starts with a period"
}

class DomainEndPeriodException: Exception() {
    override val message: String = "the domain part ends with a period"
}

class ConsecutivePeriodException: Exception() {
    override val message: String = "appear two or more consecutive periods"
}

class NoPeriodDomainException: Exception() {
    override val message: String = "no period at domain part"
}

class WrongCharLocalException(val c: Char): Exception() {
    override val message: String = "character not valid in local part ($c)"
}

class WrongCharDomainException(val c: Char): Exception() {
    override val message: String = "character not valid in domain part ($c)"
}

class WrongStartLabelException(val c: Char): Exception() {
    override val message: String = "character not valid at start of domain label ($c)"
}

class WrongEndLabelException(val c: Char): Exception() {
    override val message: String = "character not valid at end of domain label ($c)"
}
